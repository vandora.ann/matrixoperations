﻿using System;
using System.Threading.Tasks;

namespace MatrixOperations
{
    public class Matrix
    {
        private readonly double[] _data;
        private readonly int _m;
        private readonly int _n;

        public int M => _m;

        public int N => _n;

        public bool IsSquare => _m == _n;

        public Matrix(int m, int n)
        {
            _m = m;
            _n = n;
            _data = new double[m * n];
        }

        public Matrix(int m, int n, double[] data)
        {
            if (data.Length != m * n)
                throw new ArgumentException("Неверная размерность матрицы");

            _m = m;
            _n = n;
            _data = data;
        }

        public double GetElement(int i, int j)
        {
            return _data[i * _n + j];
        }

        public void SetElement(int i, int j, double element)
        {
            _data[i * _n + j] = element;
        }

        public bool IsEquals(Matrix matrix)
        {
            if (_m != matrix.M)
                return false;

            if (_n != matrix.N)
                return false;

            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };
            var result = true;
            Parallel.For(0, _m, parallelOptions, i =>
            {
                var line = i * _m;
                Parallel.For(0, _m, parallelOptions, async j =>
                {
                    if (Math.Abs(_data[line + j] - matrix._data[line + j]) > 0.00001)
                        result = false;
                });
            });
            return result;
        }

        public static Matrix operator +(Matrix matrix1, Matrix matrix2)
        {
            if (matrix1.N != matrix2.N || matrix1.M != matrix2.M)
                throw new ArgumentException("Матрицы разных размерностей");

            var count = matrix1.N * matrix1.M;
            var data = new double[count];

            var parallelOptions = new ParallelOptions { MaxDegreeOfParallelism = 4 };

            Parallel.For(0, count, parallelOptions, j => data[j] = matrix1._data[j] + matrix2._data[j]);

            return new Matrix(matrix1.M, matrix1.N, data);
        }

        public Matrix Transpose()
        {
            var data = new double[_m * _n];

            for (var i = 0; i < _m; ++i)
            {
                for (var j = 0; j < _n; ++j)
                {
                    data[j + i * _n] = _data[j * _m + i];
                }
            }

            return new Matrix(_n, _m, data);
        }

        public double CalculateDeterminant()
        {
            if (!IsSquare)
            {
                throw new InvalidOperationException(
                    "Определитель рассчитываетя только для квадратной матрицы");
            }

            if (_m == 1)
                return _data[0];

            if (_m == 2)
            {
                var det = _data[0] * _data[3] - _data[1] * _data[2];
                return det;
            }

            var result = 0.0;

            for (var j = 0; j < _m; ++j)
            {
                var det = CreateMatrixWithout(0, j).CalculateDeterminant();
                result += (j % 2 == 1 ? -1 : 1) * _data[j] * det;
            }

            return result;
        }

        private Matrix CreateMatrixWithout(int i, int j)
        {
            var elements = new double[(_m - 1) * (_n - 1)];

            var count = 0;
            for (var k = 0; k < _m; ++k)
            {
                if (k == i)
                    continue;

                for (var l = 0; l < _m; ++l)
                {
                    if (l == j)
                        continue;

                    elements[count++] = _data[k * _m + l];
                }
            }

            return new Matrix(_m - 1, _n - 1, elements);
        }
    }
}
