﻿namespace MatrixOperations
{
    public class Config
    {
        public string PathMatrix1 { get; set; }

        public string PathMatrix2 { get; set; }

        public int TypeOperation { get; set; }

        public string PathResult { get; set; }
    }
}
