﻿using System.IO;
using Newtonsoft.Json;

namespace MatrixOperations
{
    class Program
    {
        static void Main()
        {
            var config = JsonConvert.DeserializeObject<Config>(File.ReadAllText("config.json"));
            var type = config.TypeOperation;
            if (type == 1)
            {
                var operation = new Summa();
                operation.Execute(config);
            }
            if (type == 2)
            {
                var operation = new Transpose();
                operation.Execute(config);
            }
            if (type == 3)
            {
                var operation = new Determinant();
                operation.Execute(config);
            }
        }

    }
}
