﻿using System.Globalization;

namespace MatrixOperations
{
    public class Determinant : Process
    {
        internal override ResultData Counting(InputData inputData)
        {
            return new ResultData()
            {
                Scalar = inputData.Matrix1.CalculateDeterminant()
            };
        }

        internal override string FormingResponse(ResultData resultData)
        {
            return resultData.Scalar.ToString(CultureInfo.InvariantCulture);
        }
    }
}
