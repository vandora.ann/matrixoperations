﻿namespace MatrixOperations
{
    public class Transpose : Process
    {
        internal override ResultData Counting(InputData inputData)
        {
            return new ResultData()
            {
                Matrix = inputData.Matrix1.Transpose()
            };
        }

        internal override string FormingResponse(ResultData resultData)
        {
            return WriteMatrix(resultData.Matrix);
        }
    }
}
