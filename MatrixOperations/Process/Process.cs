﻿using System;
using System.IO;
using System.Globalization;
using System.Text;

namespace MatrixOperations
{
    public abstract class Process
    {
        public void Execute(Config config)
        {
            var data = ReadingData(config.PathMatrix1, config.PathMatrix2);

            var result = Counting(data);

            var str = FormingResponse(result);

            WritingFile(str, config.PathResult);
        }


        private InputData ReadingData(string pathMatrix1, string pathMatrix2)
        {
            var matrix1 = string.IsNullOrWhiteSpace(pathMatrix1) ? null : ReadMatrix(pathMatrix1);
            var matrix2 = string.IsNullOrWhiteSpace(pathMatrix2) ? null : ReadMatrix(pathMatrix2);
            return new InputData(matrix1, matrix2);
        }

        internal abstract ResultData Counting(InputData inputData);

        internal abstract string FormingResponse(ResultData resultData);

        private void WritingFile(string data, string path)
        {
            if (File.Exists(path))
                File.Delete(path);

            File.WriteAllText(path, data);
        }

        internal static string WriteMatrix(Matrix matrix)
        {
            var result = new StringBuilder();

            for (var i = 0; i < matrix.M; ++i)
            {
                for (var j = 0; j < matrix.N; ++j)
                {
                    result.Append(matrix.GetElement(i, j).ToString(CultureInfo.InvariantCulture));
                    result.Append(" ");
                }
                result.AppendLine();
            }
            return result.ToString();
        }

        private static Matrix ReadMatrix(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException();

            var lines = File.ReadAllLines(path);
            if (lines.Length < 2)
                throw new Exception("Файл не содержит корректной матрицы");

            var dims = lines[0].Split(' ');
            if (dims.Length < 1)
                throw new Exception("Файл не содержит корректной матрицы");

            try
            {
                var m = Convert.ToInt32(dims[0]);
                var n = dims.Length == 1 ? m : Convert.ToInt32(dims[1]);
                if (m * n <= 0 || lines.Length < m + 1)
                    throw new Exception("Файл не содержит корректной матрицы");

                var mat = new Matrix(m, n);
                for (var i = 0; i < m; ++i)
                {
                    var line = lines[i + 1].Split(' ');
                    if (line.Length < n)
                        throw new Exception("Файл не содержит корректной матрицы");

                    for (var j = 0; j < n; ++j)
                    {
                        mat.SetElement(i, j, Convert.ToDouble(line[j], CultureInfo.InvariantCulture));
                    }
                }
                return mat;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}
