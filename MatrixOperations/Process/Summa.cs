﻿namespace MatrixOperations
{
    public class Summa : Process
    {
        internal override ResultData Counting(InputData inputData)
        {
            return new ResultData()
            {
                Matrix = inputData.Matrix1 + inputData.Matrix2
            };
        }

        internal override string FormingResponse(ResultData resultData)
        {
            return WriteMatrix(resultData.Matrix);
        }
    }
}
