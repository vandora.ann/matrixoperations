﻿namespace MatrixOperations
{
    internal class Data
    {
        public Matrix Matrix1 { get; }
        public Matrix Matrix2 { get; }

        public Data(Matrix matrix1, Matrix matrix2)
        {
            Matrix1 = matrix1;
            Matrix2 = matrix2;
        }
    }
}
