using System.Collections.Generic;
using NUnit.Framework;

namespace MatrixOperations.Test
{
    public class TestsOperations
    {
        [Test]
        public void TestGetElement()
        {
            var mat = new Matrix(3, 2, new double[] { 1, 2, 0, 2, -1, -3 });

            Assert.AreEqual(mat.GetElement(2, 1), -3);
        }

        [Test]
        public void TestSetElement()
        {
            var mat = new Matrix(3, 2, new double[] { 1, 2, 0, 2, -1, -3 });
            var el = 5.1;
            mat.SetElement(1, 0, el);
            Assert.AreEqual(mat.GetElement(1, 0), el);
        }

        [Test]
        public void TestTranspose()
        {
            var mat = new Matrix(3, 2, new double[] { 1, 2, 0, 2, -1, -3 });
            var matT = new Matrix(2, 3, new double[] { 1, 2, 2, -1, 0, -3 });

            Assert.IsTrue(matT.IsEquals(mat.Transpose()));
        }

        [Test]
        public void TestPlus()
        {
            var mat1 = new Matrix(3, 2, new double[] { 1, 2, 0, 2, -1, -3 });
            var mat2 = new Matrix(3, 2, new double[] { 3, 2, 4, 2, 5, 7 });
            var matT = new Matrix(3, 2, new double[] { 4, 4, 4, 4, 4, 4 });

            Assert.IsTrue(matT.IsEquals(mat1 + mat2));
        }

        [Test]
        public void TestDet()
        {
            var matDet = new Dictionary<Matrix, double>()
            {
                { new Matrix(1, 1, new double[] { -3 }), -3 },
                { new Matrix(2, 2, new double[] { 1, 2, -1, -3 }), -1 },
                { new Matrix(3, 3, new double[] { 1, 2, -1, -3, 1, 2, 0, 2, -1 }), -5 },
                { new Matrix(4, 4, new double[] { 1, 2, 1, 2, -1, -3, 1, 2, 0, 2, -1, - 1, -3, 0, 2, -1 }), 32},
            };

            foreach (var mat in matDet)
            {
                Assert.AreEqual(mat.Key.CalculateDeterminant(), mat.Value);
            }
        }
    }
}