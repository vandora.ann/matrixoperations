﻿using System;
using System.IO;
using NUnit.Framework;

namespace MatrixOperations.Test
{
    public class TestProcess
    {
        private Config _config = new Config()
        {
            PathMatrix1 = Path.Combine(AppContext.BaseDirectory, "Data", "matrix1.txt"),
            PathMatrix2 = Path.Combine(AppContext.BaseDirectory, "Data", "matrix2.txt"),
            PathResult = Path.Combine(AppContext.BaseDirectory, "result.txt"),
        };

        [Test]
        public void ProcessSumma()
        {
            var summa = new Summa();
            summa.Execute(_config);
            var result = "2 -62 6 -4 \r\n-3 0 4.6 0 \r\n-2 3 4.3 -1 \r\n4 -3 3.3 -1.9 \r\n";
            Assert.AreEqual(File.ReadAllText(_config.PathResult), result);
        }

        [Test]
        public void ProcessTranspose()
        {
            var transpose = new Transpose();
            transpose.Execute(_config);
            var result = "1 -3 1 3 \r\n-21 2 1 -2 \r\n3 1.3 3 3.3 \r\n-4 2.1 -4 2.1 \r\n";
            Assert.AreEqual(File.ReadAllText(_config.PathResult), result);
        }

        [Test]
        public void ProcessDeterminant()
        {
            var det = new Determinant();
            det.Execute(_config);
            var result = "-1953.6";
            Assert.AreEqual(File.ReadAllText(_config.PathResult), result);
        }
    }
}
